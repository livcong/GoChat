package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/gorilla/websocket"
	"time"
)

type WebSocketController struct {
	beego.Controller
}

type WsUser struct {
	Name string
	Conn *websocket.Conn
}

type Msg struct {
	Type string			`json:"type"`
	Content string		`json:"content"`
	Timestamp int		`json:"timestamp"`
}

var (
	WsUserList = make(chan WsUser)
	exUserList = make(chan WsUser)
	msgList = make(chan Msg)
	UserList = make(map [WsUser]bool)
	upgrader = websocket.Upgrader{}
)

func (this *WebSocketController) Join() {

	name := this.GetString("name")

	ws, err := upgrader.Upgrade(this.Ctx.ResponseWriter, this.Ctx.Request, nil);
	if err != nil {
		return
	}

	user := WsUser{name, ws}
	WsUserList <- user

	_ = ws.WriteJSON(CreateMsg("login_success", "success"))

	defer func() {
		exUserList <- user
	}()

	for {
		_, p, err := ws.ReadMessage()
		if err != nil {
			fmt.Println(err)
			break;
		}
		msgList <- CreateMsg("chat", name+": "+string(p))
	}

	this.Ctx.WriteString("end")
}

func CreateMsg( Type string, Content string ) Msg {
	return Msg{Type, Content, int(time.Now().Unix())}
}

func ChatRoom () {
	for {
		select {
		case user := <- WsUserList :

			UserList[user] = true

		case user := <- exUserList :

			delete(UserList, user)

		case msg := <- msgList :

			for user := range UserList {
				if err := user.Conn.WriteJSON(msg); err != nil {
					fmt.Println(err)
				}
			}
		}

	}
}

func init() {
	go ChatRoom()
}