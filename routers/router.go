package routers

import (
	"github.com/astaxie/beego"
	"../controllers"
)

func init() {
	beego.Router("/", &controllers.IndexController{})
	beego.Router("/ws/join", &controllers.WebSocketController{}, "get:Join")

}